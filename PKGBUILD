# Maintainer: Klaus-Dieter Schmatz <tucuxir at sapo dot pt>
pkgname=dwm
pkgver=6.4
pkgrel=1
pkgdesc='A customized build of the Dynamic Window Manager'
arch=('x86_64')
url='https://dwm.suckless.org'
license=('MIT')
depends=('libxinerama' 'libxft')
optdepends=('alacritty: terminal support'
            'rofi: application launcher support')
source=("https://dl.suckless.org/$pkgname/$pkgname-$pkgver.tar.gz"
        "dwm-hide_vacant_tags-6.3.diff"
        "dwm-titlecolor-20210815-ed3ab6b4.diff"
        "dwm-uselessgap-20211119-58414bee958f2.diff")
sha256sums=('fa9c0d69a584485076cfc18809fd705e5c2080dafb13d5e729a3646ca7703a6e'
            '41292f5c1663ab61c6cbacdb451548461556f9fc10269723eb6b955469710c31'
            '08974ad6fb8f32938b7a793ecc6e281c7bbdfe8f40d1ed52c2ef31f62738f5c6'
            '80cb7a75ae1f38fe7ca167d636fb8e90506dddd6165c2f32cbb0dd1b02eff576')
_sourcedir="$pkgname-$pkgver"
_makeopts="--directory=$_sourcedir"

prepare() {
  cat *.diff | patch --directory="$_sourcedir" --strip=1

  # This package provides a mechanism to provide a custom config.h. Multiple
  # configuration states are determined by the presence of two files in
  # $BUILDDIR:
  #
  # config.h  config.def.h  state
  # ========  ============  =====
  # absent    absent        Initial state. The user receives a message on how
  #                         to configure this package.
  # absent    present       The user was previously made aware of the
  #                         configuration options and has not made any
  #                         configuration changes. The package is built using
  #                         default values.
  # present                 The user has supplied his or her configuration. The
  #                         file will be copied to $srcdir and used during
  #                         build.
  #
  # After this test, config.def.h is copied from $srcdir to $BUILDDIR to
  # provide an up to date template for the user.
  if [ -e "$BUILDDIR/config.h" ]; then
    cp "$BUILDDIR/config.h" "$_sourcedir"
  elif [ ! -e "$BUILDDIR/config.def.h" ]; then
    msg='This package can be configured in config.h. Copy the config.def.h '
    msg+='that was just placed into the package directory to config.h and '
    msg+='modify it to change the configuration. Or just leave it alone to '
    msg+='continue to use default values.'
    echo "$msg"
  fi
  cp "$_sourcedir/config.def.h" "$BUILDDIR"
}

build() {
  make $_makeopts X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
  local installopts='--mode 0644 -D --target-directory'
  licdir="$pkgdir/usr/share/licenses/$pkgname" 
  make $_makeopts PREFIX=/usr DESTDIR="$pkgdir" install
  install $installopts "$licdir" "$_sourcedir/LICENSE" 
}

# vim:set ts=2 sw=2 et:
